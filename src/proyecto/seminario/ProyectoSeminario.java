package proyecto.seminario;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class ProyectoSeminario {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException{
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        new Login().setVisible(true);
    }
    
}
